-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2018 at 05:54 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `attendance`
--

-- --------------------------------------------------------

--
-- Table structure for table `att_tbl_department`
--

CREATE TABLE `att_tbl_department` (
  `department_id` int(11) NOT NULL,
  `department_name` text NOT NULL,
  `department_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `att_tbl_event`
--

CREATE TABLE `att_tbl_event` (
  `event_id` int(11) NOT NULL,
  `event_name` varchar(255) NOT NULL,
  `event_date` datetime(6) NOT NULL,
  `event_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `att_tbl_section`
--

CREATE TABLE `att_tbl_section` (
  `section_id` int(11) NOT NULL,
  `section_name` varchar(255) NOT NULL,
  `department_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `att_tbl_student`
--

CREATE TABLE `att_tbl_student` (
  `student_id` int(11) NOT NULL,
  `student_firstname` varchar(255) NOT NULL,
  `student_midname` varchar(255) NOT NULL,
  `student_lastname` varchar(255) NOT NULL,
  `year_level` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `time_in` datetime(6) NOT NULL,
  `time_out` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `att_tbl_teacher`
--

CREATE TABLE `att_tbl_teacher` (
  `teacher_id` int(11) NOT NULL,
  `teacher_name` varchar(255) NOT NULL,
  `department_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `att_tbl_time`
--

CREATE TABLE `att_tbl_time` (
  `id` int(11) NOT NULL,
  `time_in` datetime(6) NOT NULL,
  `time_out` datetime(6) NOT NULL,
  `event_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `att_tbl_user`
--

CREATE TABLE `att_tbl_user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_profilepic` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `att_tbl_department`
--
ALTER TABLE `att_tbl_department`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `att_tbl_event`
--
ALTER TABLE `att_tbl_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `att_tbl_section`
--
ALTER TABLE `att_tbl_section`
  ADD PRIMARY KEY (`section_id`);

--
-- Indexes for table `att_tbl_student`
--
ALTER TABLE `att_tbl_student`
  ADD PRIMARY KEY (`student_id`);

--
-- Indexes for table `att_tbl_teacher`
--
ALTER TABLE `att_tbl_teacher`
  ADD PRIMARY KEY (`teacher_id`);

--
-- Indexes for table `att_tbl_time`
--
ALTER TABLE `att_tbl_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `att_tbl_user`
--
ALTER TABLE `att_tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `att_tbl_department`
--
ALTER TABLE `att_tbl_department`
  MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `att_tbl_event`
--
ALTER TABLE `att_tbl_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `att_tbl_section`
--
ALTER TABLE `att_tbl_section`
  MODIFY `section_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `att_tbl_student`
--
ALTER TABLE `att_tbl_student`
  MODIFY `student_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `att_tbl_teacher`
--
ALTER TABLE `att_tbl_teacher`
  MODIFY `teacher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `att_tbl_time`
--
ALTER TABLE `att_tbl_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `att_tbl_user`
--
ALTER TABLE `att_tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
