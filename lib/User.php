<?php
	include_once 'Session.php';
	include 'Database.php';
	include('phpqrcode/qrlib.php'); 

	class User{
		private $db;
		public function __construct(){
			$this->db = new Database(); 
		}

		// public function generateQrCode($student_id,$student_name,$qr_value)
		public function generateQrCode($student_id,$student_name)
		{
			$tempDir = 'uploads/'; 
		    // $codeContents = $qr_value;
		    $codeContents = 'http://192.168.2.101/attendance_system/attendance.php?id='.$student_id;  
		    $fileName = $student_name.$student_id.'.png';
		    $pngAbsoluteFilePath = $tempDir.$fileName; 
		    $urlRelativeFilePath = $tempDir.$fileName; 

		     if (!file_exists($pngAbsoluteFilePath)) { 
        		QRcode::png($codeContents, $pngAbsoluteFilePath); 
        		return $pngAbsoluteFilePath;
		    } else { 
		        // echo 'File already generated! We can use this cached file to speed up site on common codes!'; 
		    } 
		}
		public function userRegistration($data){

			$student_id = $data['student_id'];
			$student_name = $data['student_name'];
		  	$qr_value  = $data['qr_value'];
			$chk_id = $this->idCheck($student_id);

			//Generate QR Code
			// $get_qr = $this->generateQrCode($student_id,$student_name,$qr_value);
			$get_qr = $this->generateQrCode($student_id,$student_name);
			if ($student_id == "" OR $student_name == "") {
				$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Field must not be Empty</div>";
				return $msg;
			}elseif (strlen($student_name) < 3) {
				$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Student Name is too Short!</div>";
				return $msg;
			} 

			if ($chk_id == true) {
				$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Student already Exist!</div>";
				return $msg;
			}

			$sql = "INSERT INTO tbl_student (student_id,student_name,QR) VALUES(:student_id,:student_name,:QR)";
			$query = $this->db->pdo->prepare($sql); 
			$query->bindValue(':student_id', $student_id);
			$query->bindValue(':student_name', $student_name);
			$query->bindValue(':QR', $get_qr);
			$result = $query->execute();
			if ($result) {
				$msg = "<div class='alert alert-success'><strong>Success ! </strong>Thank you, Student Added</div>";
				return $msg;
			}else{
				$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Sorry, there has been problem inserting your details.</div>";
				return $msg;
			}
				
		}

		public function idCheck($student_id){
			$sql = "SELECT student_id FROM tbl_student WHERE student_id = :student_id";
			$query = $this->db->pdo->prepare($sql); 
			$query->bindValue(':student_id', $student_id);
			$query->execute();
			if ($query->rowCount() > 0) {
					return true;
				}else{
					return false;
			}
		}

		public function getLoginUser($email, $password){
			$sql = "SELECT * FROM tbl_user WHERE email = :email AND password = :password LIMIT 1";
			$query = $this->db->pdo->prepare($sql); 
			$query->bindValue(':email', $email);
			$query->bindValue(':password', $password);
			$query->execute();
			$result = $query->fetch(PDO::FETCH_OBJ);
			return $result;
		}

		public function userLogin($data){
			$email    = $data['email'];
			$password = $data['password'];

			$chk_email = $this->emailCheck($email);

			if ($email == "" OR $password == "") {
				$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Field must not be Empty</div>";
				return $msg;
			}

			if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
				$msg = "<div class='alert alert-danger'><strong>Error ! </strong>The email address is not valid!</div>";
				return $msg;
			}

			if ($chk_email == false) {
				$msg = "<div class='alert alert-danger'><strong>Error ! </strong>The email address Not Exist!</div>";
				return $msg;
			}

			$result = $this->getLoginUser($email, $password);
			if ($result) {
				Session::init();
				Session::set("login", true);
				Session::set("id", $result->id);
				Session::set("name", $result->name);
				Session::set("username", $result->username);
				Session::set("loginmsg", "<div class='alert alert-success'><strong>Success ! </strong>You are LoggedIn!</div>");
				header("Location: index.php");
			}else{
				$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Data not found!</div>";
				return $msg;
			}
		}

		public function getUserData(){
			$sql = "SELECT * FROM tbl_student ORDER BY id DESC";
			$query = $this->db->pdo->prepare($sql); 
			$query->execute();
			$result = $query->fetchAll();
			return $result;
		}

		public function getUserById($id){
			$sql = "SELECT * FROM tbl_student WHERE id = :id LIMIT 1";
			$query = $this->db->pdo->prepare($sql);
			$query->bindValue(':id', $id);
			$query->execute();
			$result = $query->fetch(PDO::FETCH_OBJ);
			return $result;
		}

	// public function updateUserData($id, $data, $qr_value){
	public function updateUserData($id, $data){
			$qrPath			  = $data['qrPath'];
			$student_id  	  = $data['student_id'];
			$student_name	  = $data['student_name'];
			// $get_qr = $this->generateQrCode($student_id,$student_name,$qr_value);
			$get_qr = $this->generateQrCode($student_id,$student_name);
			if ($student_id == "" OR $student_name == "") {
				$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Field Must Not be Empty</div>";
				return $msg;
			}

			unlink($qrPath);
			$sql = "UPDATE tbl_student set
						student_id 	 = :student_id,
						student_name = :student_name,
						QR = :QR
						WHERE id = :id";

			$query = $this->db->pdo->prepare($sql);

			$query->bindValue(':student_id', $student_id);
			$query->bindValue(':student_name', $student_name);	
			$query->bindValue(':QR', $get_qr);	
			$query->bindValue(':id', $id);
			$result = $query->execute();
			if ($result) {
				$msg = "<div class='alert alert-success'><strong>Success ! </strong>Userdata updated Succesfully.</div>";
				return $msg;
			}else{
				$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Userdata Not Updated.</div>";
				return $msg;
			}
		}

		private function checkPassword($id, $old_pass){
			$password = md5($old_pass);
			$sql = "SELECT password FROM tbl_user WHERE id = :id AND password = :password";
			$query = $this->db->pdo->prepare($sql); 
			$query->bindValue(':id', $id);
			$query->bindValue(':password', $password);
			$query->execute();
			if ($query->rowCount() > 0) {
					return true;
				}else{
					return false;
		}
	}

		public function updatePassword($id, $data){
			$old_pass = $data['old_pass'];
			$new_pass = $data['password'];
			$chk_pass = $this->checkPassword($id, $old_pass);

			if ($old_pass == "" OR $new_pass == ""){
				$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Field Must Not be Empty.</div>";
				return $msg;
			}
			
			if ($chk_pass == false){
				$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Old Password Not Exist.</div>";
			return $msg;
				} 

				if (strlen($new_pass) < 6){
					$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Password is to short.</div>";
			return $msg;
				}

				$password = md5($new_pass);
				$sql = "UPDATE tbl_user set
						password = :password
						WHERE id = :id";

			$query = $this->db->pdo->prepare($sql);
			 
			$query->bindValue(':password', $password);
			$query->bindValue(':id', $id);
			$result = $query->execute();
			if ($result) {
				$msg = "<div class='alert alert-success'><strong>Success ! </strong>Password updated Succesfully.</div>";
				return $msg;
			}else{
				$msg = "<div class='alert alert-danger'><strong>Error ! </strong>Password Not Updated.</div>";
				return $msg;
			}
			
		}
	}
	?>