<?php
    include 'lib/User.php';
    include 'inc/header.php';
    // include 'phpqrcode/qrlib.php'; 
?>

<?php 
if (isset($_GET['id'])) {
   $userid = (int)$_GET['id'];
}

 $user = new User();
 if(isset($_POST['update'])){
     $updateusr = $user->updateUserData($userid, $_POST);
   }
  ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h2>User Profile <span class="pull-right"><a class="btn btn-primary" href="index.php">Back</a></span></h2>
    </div>

    <div class="panel-body">
      <div style="max-width: 600px; margin: 0 auto">
<?php  
  if (isset($updateusr)) {
  echo $updateusr;
}
?>
<?php 
      $userdata = $user->getUserById($userid);
      if ($userdata){
        $qrPath = $userdata->QR;
?>
<div class="col-md-3">
            <div class="form-group">
               <center><label>QR code</label></center>
               <img src="<?php echo $qrPath; ?>" width="100%">
                
           </div>

</div>
<div class="col-md-9">
       <form action="" method="POST">

           <div class="form-group">
               <label for="student_id">Student ID</label>
               <input type="student_id" id="student_id" name="student_id" class="form-control" value="<?php echo $userdata->student_id;?>" />
           </div>

           <div class="form-group">
               <label for="student_name">Student student_id</label>
               <input type="student_name" id="student_name" name="student_name" class="form-control" value="<?php echo $userdata->student_name; ?>" />
           </div>

           <!--  <div class="form-group">
               <label for="student_name">QR value</label>
               <input type="text" id="qr_value" name="qr_value" class="form-control"/>
           </div> -->
            <input type="hidden" id="qrPath" name="qrPath" class="form-control" value="<?php echo $qrPath;?>" />
            <button type="submit" name="update" class="btn btn-success">
            Update</button>
       </form>
     <?php } ?>
       </div>
     </div>
    </div>
</div>
<?php include 'inc/footer.php';
?>