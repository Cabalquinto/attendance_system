

SOURCES
https://sourceforge.net/projects/phpqrcode/files/ - phpQrcode library 
http://phpqrcode.sourceforge.net/

------------------------------------------------------------------------------------------------------

REQUIREMENTS:
phpQrcode library   - https://sourceforge.net/projects/phpqrcode/files/
QR CODE scanner apk - https://play.google.com/store/apps/details?id=com.gamma.scan&hl=en_US
QR GENERATOR	    - https://www.qr-code-generator.com/
XAMPP		        - https://www.apachefriends.org/index.html

-------------------------------------------------------------------------------------------------------
Instruction:
1. Import tbl_student.sql and att_tbl_time.sql in your database (database_name: test)
2. Put your project inside the htdocs folder and share the folder to the network.
3. Get your IP address using command prompt.
4. Go to lib/User.php and locate $codeContents change the ip address with your ip address
5. Generate QR code using register.php
6. Scan the QR code and open the link (time in)
7. re-Scan the Qr code open the link (time out)

