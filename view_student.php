 <?php
    include 'lib/User.php';
    include 'inc/header.php';
?>

<div class="panel panel-default">
    <div class="panel-body">
        <table class="table table-striped">
            <th width="33%">Student ID</th>
            <th width="33%">Student Name</th>
            <th width="33%">Qr Code</th>
      
<?php  
    $user = new User();
    $userdata = $user->getUserData();
    if ($userdata) {
        $i = 0;
        foreach ($userdata as $sdata) {
            $i++;
?>
            <tr>
                <td><?php echo $sdata['student_id']; ?></td>
                <td><?php echo $sdata['student_name']; ?></td>
                <td><img src="<?php echo $sdata['QR']; ?>" width="20%"></td>
                <td>
                    <a class="btn btn-primary" href="profile.php?id=<?php echo $sdata['id']; ?>">Edit</a>
                </td>
            </tr> 
<?php } }else{ ?>
    <tr><td colspan="5"><h2>No User Data Found....</h2></td></tr>
<?php } ?>
        </table>
    </div>
</div>
<?php include 'inc/footer.php';
?>

  