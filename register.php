<?php
    include 'inc/header.php';
    include 'lib/User.php';
?>
<?php
  $user = new User();
  if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['register'])){
     $usrRegi = $user->userRegistration($_POST);
  }
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <center><h2>Generate Qr CODE</h2></center>
    </div>

    <div class="panel-body">
      <div style="max-width:600px; margin:0 auto">
<?php
if (isset($usrRegi)) {
    echo $usrRegi;
    header('location: view_student.php');
  }
?>
       <form action="" method="POST">

           <div class="form-group">
               <label for="student_id">Student Id</label>
               <input type="text" id="student_id" name="student_id" class="form-control"/>
           </div>

           <div class="form-group">
               <label for="student_name">Student Name</label>
               <input type="student_name" id="student_name" name="student_name" class="form-control"/>
           </div>
           <!--  <div class="form-group">
               <label for="student_name">QR value</label>
               <input type="text" id="qr_value" name="qr_value" class="form-control"/>
           </div> -->
            <button type="submit" name="register" class="btn btn-success">Submit</button>
       </form>

       </div>
    </div>
</div>
<?php include 'inc/footer.php';
