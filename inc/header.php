 <?php
    $filepath = realpath(dirname(__FILE__));
    include_once $filepath.'/../lib/Session.php';
    Session::init();
?>
<!DOCTYPE html>
<html> 
<head>
    <title>Registration form</title>
    <link rel="stylesheet" href="inc/bootstrap.min.css"/>
    <script src="inc/jquery.min.js"></script>
    <script src="inc/bootstrap.min.css"></script>
</head>
<?php
    if (isset($_GET['action']) && $_GET['action'] == "logout") {
        Session::destroy();
    }
?>
<body>
<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
             <div class="navbar-header">
                 <button class="btn btn-primary"><a class="btn btn-primary" href="register.php">Add Student</a></button>
             </div>
             <ul class="nav navbar-nav pull-right">
                 <button class="btn btn-primary"><a class="btn btn-primary" href="view_student.php">Back</a></button>
             </ul>
        </div>
    </nav>
    